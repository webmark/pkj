<?php
class PkjNo extends PkjCoreChild {
	public function setup() {
		register_post_type ( 'references', array (
				'labels' => array (
						'name' => __ ( 'References' ),
						'singular_name' => __ ( 'Reference' ),
						'add_new' => 'Add Reference',
						'add_new_item' => __ ( 'Add New References' ),
						'edit_item' => __ ( 'Edit Reference' ),
						'new_item' => __ ( 'New Reference' ),
						'view_item' => __ ( 'Show Reference' ),
						'search_items' => __ ( 'Search in References' ),
						'not_found' => __ ( 'No References Found' ),
						'not_found_in_trash' => __ ( 'No References Found in Trash' ),
						'parent' => __ ( 'Parent Reference' ) 
				),
				
				'public' => true,
				'menu_position' => 15,
				'supports' => array (
						'title',
						'thumbnail',
						'editor' 
				),
				'taxonomies' => array (),
				'has_archive' => true,
				'rewrite' => array (
						'slug' => 'referanse' 
				) 
		) );
		
		PkjCore::getInstance ()->registerMetaBox ( 'references_info', 'Lokasjon' );
		PkjCore::getInstance ()->registerPostField ( 'text', 'references', 'site_url', array (
			'label' => __('Site URL')
		), 'references_info' );
		PkjCore::getInstance ()->registerPostField ( 'text', 'references', 'year_from', array (
			'label' => __('Year From')
		), 'references_info' );
		PkjCore::getInstance ()->registerPostField ( 'text', 'references', 'year_to', array (
			'label' => __('Year To')
		), 'references_info' );
		
		
		
		register_post_type ( 'projects', array (
		'labels' => array (
		'name' => __ ( 'Projects' ),
		'singular_name' => __ ( 'Project' ),
		'add_new' => 'Add Project',
		'add_new_item' => __ ( 'Add New Projects' ),
		'edit_item' => __ ( 'Edit Project' ),
		'new_item' => __ ( 'New Project' ),
		'view_item' => __ ( 'Show Project' ),
		'search_items' => __ ( 'Search in Projects' ),
		'not_found' => __ ( 'No Projects Found' ),
		'not_found_in_trash' => __ ( 'No Projects Found in Trash' ),
		'parent' => __ ( 'Parent Project' )
		),
		
		'public' => true,
		'menu_position' => 15,
		'supports' => array (
		'title',
		'thumbnail',
		'editor'
				),
				'taxonomies' => array (),
				'has_archive' => true,
				'rewrite' => array (
				'slug' => 'prosjekt'
						)
		) );
		
		
		
		
	}
}
