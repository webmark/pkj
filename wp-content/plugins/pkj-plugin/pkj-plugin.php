<?php
/**
 * 
 * @package   PKJ - Betcon AS
 * @author    Petter Kjelkenes <kjelkenes@gmail.com>
 * @license   LGPL
 * @link      http://pkj.no
 * @copyright 2013 Petter Kjelkenes ENK
 *
 * @wordpress-plugin
 * Plugin Name: PKJ - PKJ
 * Plugin URI:  http://pkj.no
 * Description: Logic for PKJ AS
 * Version:     1.0.0
 * Author:      Petter Kjelkenes ENK
 * Author URI:  http://pkj.no
 * Text Domain: Betcon
 * License:     LGPL
 */



add_filter( 'pkj-base-loaded', function () {
	$name = 'pkj - Webmark';
	$ns = 'PkjNo';
	$dependencies =array('PkjCore');
	
		
	// -- Bootstrap --
	if (class_exists('PkjCore')) {
		require dirname(__FILE__) . "/lib/$ns.php";
		$pkjCore = PkjCore::getInstance();
		$pkjCore->registerChild(new $ns(
				__DIR__,
				$ns,
				// Dependencies
				$dependencies
		));
	} else {
		add_action( 'admin_notices', function () use ($name) {
			echo sprintf('<div class="error"><p>PKJ - Core plugin is needed for %s</p></div>', $name);
		});
	}
		
} );				
