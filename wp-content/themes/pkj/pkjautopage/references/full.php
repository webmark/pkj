<?php 
$options = shortcode_atts(array(
	'show_the_title' => true,
	'show_author_info' => true,
	'show_featured_graphic' => true,
	'show_tags' => true,
	'show_comments' => false
), $options);

// Must have thumbnail..
$options['show_featured_graphic'] = $options['show_featured_graphic'] &&  has_post_thumbnail();

?>
	<?php if ($options['show_the_title']):?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	<?php endif?>	
	
	
	<?php if ( $options['show_featured_graphic']):?>
		<div class="media-object">
			<?php the_post_thumbnail('full')?>
		</div>
	<?php endif?>	
	
	<div class="the_content <?php if ($options['show_featured_graphic']) {echo "the_content_has_graphic";}?>">
		<?php the_content( ); ?>
	</div>
	
	<?php if ($options['show_tags']):?>
	<?php the_tags()?>
	<?php endif?>
	<?php if ( is_singular() && get_the_author_meta( 'description' ) && $options['show_author_info'] ) : ?>
		<div class="author-info media">
			<a class="pull-left" href="#"> <?php echo get_avatar( get_the_author_meta( 'user_email' ), 68 ); ?>
			</a>
			<div class="media-body">
				<h4 class="media-heading"><?php printf( __( 'About %s', 'pkj' ), get_the_author() ); ?></h4>
				<p><?php the_author_meta( 'description' ); ?></p>
				<a
						href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"
						rel="author">
									<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentytwelve' ), get_the_author() ); ?>
								</a>
			</div>
		</div>
	<?php endif; ?>
	
	
	<?php if ($options['show_comments']):?>
		<?php comments_template()?>
	<?php endif?>
	<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'pkj' ), 'after' => '</div>' ) ); ?>
	